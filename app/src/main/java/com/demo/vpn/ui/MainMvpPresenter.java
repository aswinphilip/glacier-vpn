package com.demo.vpn.ui;

import com.demo.vpn.ui.base.MvpPresenter;
import com.demo.vpn.ui.base.MvpView;

/**
 * Created by cedex on 1/23/2018.
 */

public interface MainMvpPresenter<V extends MvpView> extends MvpPresenter<V>{

    void loadText(String text);

    void loadDummyContent();

    void loadIp();
}
