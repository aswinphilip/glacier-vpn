package com.demo.vpn.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.demo.vpn.R;
import com.demo.vpn.di.component.ActivityComponent;
import com.demo.vpn.service.MyVpnService;
import com.demo.vpn.ui.base.BaseActivity;
import com.demo.vpn.utils.NetworkUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainMvpView{

    @BindView(R.id.text)
    protected TextView text;
    @BindView(R.id.connect)
    protected Button connect;
    @BindView(R.id.disconnect)
    protected Button disconnect;

    @Inject
    MainPresenter<MainMvpView> mPresenter;


    public static final String SERVER_ADDRESS = "102.248.59.23";
    public static final String SERVER_PORT = "8080";
    public static final String SHARED_SECRET = "qWe12345!@#$%";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActivityComponent component = getActivityComponent();
        if(component != null){
            component.inject(this);
            ButterKnife.bind(this);
            mPresenter.onAttach(this);
        }
        setUp();
    }

    @Override
    protected void setUp() {
        final SharedPreferences prefs = getSharedPreferences(Prefs.NAME, MODE_PRIVATE);

        mPresenter.loadIp();

        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prefs.edit()
                        .putString(Prefs.SERVER_ADDRESS, SERVER_ADDRESS)
                        .putString(Prefs.SERVER_PORT, SERVER_PORT)
                        .putString(Prefs.SHARED_SECRET, SHARED_SECRET)
                        .commit();
                Intent intent = MyVpnService.prepare(MainActivity.this);
                if (intent != null) {
                    startActivityForResult(intent, 0);
                } else {
                    onActivityResult(0, RESULT_OK, null);
                }
            }
        });


        disconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startService(getServiceIntent().setAction(MyVpnService.ACTION_DISCONNECT));
            }
        });
    }

    private Intent getServiceIntent() {
        return new Intent(this, MyVpnService.class);
    }

    @Override
    public void setText(String msg) {
        text.setText("Current IP Address is "+ msg);
    }

    public interface Prefs {
        String NAME = "connection";
        String SERVER_ADDRESS = "server.address";
        String SERVER_PORT = "server.port";
        String SHARED_SECRET = "shared.secret";
    }



    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            startService(getServiceIntent().setAction(MyVpnService.ACTION_CONNECT));
        }
    }
}
