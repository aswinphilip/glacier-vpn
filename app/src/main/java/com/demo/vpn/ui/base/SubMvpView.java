package com.demo.vpn.ui.base;

/**
 * Created by cedex on 1/23/2018.
 */

public interface SubMvpView extends MvpView {
    void onCreate();

    void onStart();

    void onResume();

    void onPause();

    void onStop();

    void onDestroy();

    void attachParentMvpView(MvpView mvpView);
}
