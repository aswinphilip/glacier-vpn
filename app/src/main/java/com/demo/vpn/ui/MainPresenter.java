package com.demo.vpn.ui;

import com.demo.vpn.data.DataManager;
import com.demo.vpn.data.network.error.ANError;
import com.demo.vpn.data.network.model.DummyResponse;
import com.demo.vpn.data.network.model.IPAddress;
import com.demo.vpn.ui.base.BasePresenter;
import com.demo.vpn.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by cedex on 1/23/2018.
 */

public class MainPresenter<V extends MainMvpView> extends BasePresenter<V>
                        implements MainMvpPresenter<V>{

    @Inject
    public MainPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }

    @Override
    public void loadText(String text) {
        getMvpView().setText(text);
        loadDummyContent();
    }

    @Override
    public void loadDummyContent() {
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager()
                    .getDummyResponse()
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(new Consumer<DummyResponse>() {
                        @Override
                        public void accept(DummyResponse dummyResponse) throws Exception {
                            if (dummyResponse != null) {
                                getMvpView().setText(dummyResponse.getMessage());
                            }
                            getMvpView().hideLoading();
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            if(!isViewAttached()){
                                return;
                            }
                            getMvpView().hideLoading();
                            if(throwable instanceof ANError){
                                ANError anError = (ANError) throwable;
                                handleApiError(anError);
                            }
                        }
                    }));
    }

    @Override
    public void loadIp() {
        //getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager()
                .getIpAddress()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<IPAddress>() {
                    @Override
                    public void accept(IPAddress ipAddress) throws Exception {
                        if (ipAddress != null) {
                            getMvpView().setText(ipAddress.getIp());
                        }
                        getMvpView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        if(!isViewAttached()){
                            return;
                        }
                        getMvpView().hideLoading();
                        if(throwable instanceof ANError){
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }
}
