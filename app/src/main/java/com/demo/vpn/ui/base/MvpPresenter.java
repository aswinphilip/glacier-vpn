package com.demo.vpn.ui.base;

import com.demo.vpn.data.network.error.ANError;

/**
 * Created by cedex on 1/23/2018.
 */

public interface MvpPresenter<V extends MvpView> {

    void onAttach(V mvpView);

    void onDetach();

    void handleApiError(ANError error);

    void setUserAsLoggedOut();
}
