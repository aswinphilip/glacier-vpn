package com.demo.vpn.ui;

import com.demo.vpn.ui.base.MvpView;

/**
 * Created by cedex on 1/23/2018.
 */

public interface MainMvpView extends MvpView{

    void setText(String text);
}
