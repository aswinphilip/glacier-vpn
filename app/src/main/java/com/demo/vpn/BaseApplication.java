package com.demo.vpn;

import android.app.Application;
import android.content.Context;

import com.demo.vpn.data.DataManager;
import com.demo.vpn.di.component.ApplicationComponent;
import com.demo.vpn.di.component.DaggerApplicationComponent;
import com.demo.vpn.di.module.ApplicationModule;
import com.demo.vpn.utils.AppLogger;

import javax.inject.Inject;

/**
 * Created by cedex on 1/23/2018.
 */

public class BaseApplication extends Application {

    @Inject
    DataManager mDataManager;

    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();
        mApplicationComponent.inject(this);

        AppLogger.init();
    }

    public static BaseApplication get(Context context) {
        return (BaseApplication) context.getApplicationContext();
    }

    public ApplicationComponent getApplicationComponent(){
        return mApplicationComponent;
    }

    public void setApplicationComponent(ApplicationComponent applicationComponent){
        this.mApplicationComponent = applicationComponent;
    }
}
