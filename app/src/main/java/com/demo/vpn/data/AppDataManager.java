package com.demo.vpn.data;

import android.content.Context;

import com.demo.vpn.data.db.DbHelper;
import com.demo.vpn.data.network.ApiHelper;
import com.demo.vpn.data.network.model.DummyResponse;
import com.demo.vpn.data.network.model.IPAddress;
import com.demo.vpn.data.pref.PreferencesHelper;
import com.demo.vpn.di.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by cedex on 1/23/2018.
 */
@Singleton
public class AppDataManager implements DataManager {

    private static final String TAG = "AppDataManager";

    private final Context mContext;
    private final DbHelper mDbHelper;
    private final PreferencesHelper mPreferencesHelper;
    private final ApiHelper mApiHelper;

    @Inject
    public AppDataManager(@ApplicationContext Context context,
                          DbHelper dbHelper,
                          PreferencesHelper preferencesHelper,
                          ApiHelper apiHelper) {
        mContext = context;
        mDbHelper = dbHelper;
        mPreferencesHelper = preferencesHelper;
        mApiHelper = apiHelper;
    }

    @Override
    public Observable<DummyResponse> getDummyResponse() {
        return mApiHelper.getDummyResponse();
    }

    @Override
    public Observable<IPAddress> getIpAddress() {
        return mApiHelper.getIpAddress();
    }
}
