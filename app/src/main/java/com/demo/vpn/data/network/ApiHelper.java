package com.demo.vpn.data.network;

import com.demo.vpn.data.network.model.DummyResponse;
import com.demo.vpn.data.network.model.IPAddress;

import io.reactivex.Observable;


/**
 * Created by cedex on 1/23/2018.
 */

public interface ApiHelper {

   Observable<DummyResponse> getDummyResponse();

   Observable<IPAddress> getIpAddress();
}
