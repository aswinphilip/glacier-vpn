package com.demo.vpn.data.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cedex on 1/23/2018.
 */

public class DummyResponse {
    @SerializedName("status")
    private String status;

    @SerializedName("msg")
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
