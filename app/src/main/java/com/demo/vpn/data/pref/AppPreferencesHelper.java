package com.demo.vpn.data.pref;

import android.content.Context;
import android.content.SharedPreferences;

import com.demo.vpn.di.ApplicationContext;
import com.demo.vpn.di.PreferenceInfo;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by cedex on 1/23/2018.
 */
@Singleton
public class AppPreferencesHelper implements PreferencesHelper {

    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferencesHelper(@ApplicationContext Context context,
                                @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }
}
