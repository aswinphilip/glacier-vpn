package com.demo.vpn.data.network;

import com.demo.vpn.data.network.model.DummyResponse;
import com.demo.vpn.data.network.model.IPAddress;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by cedex on 1/23/2018.
 */

public interface ApiInterface {

    @GET("hello_world")
    Observable<DummyResponse> getDummyContent();

    @GET("/")
    Observable<IPAddress> getIpAddress(
            @Query("format") String format
    );
}
