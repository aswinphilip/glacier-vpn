package com.demo.vpn.data;

import com.demo.vpn.data.db.DbHelper;
import com.demo.vpn.data.network.ApiHelper;
import com.demo.vpn.data.pref.PreferencesHelper;

/**
 * Created by cedex on 1/23/2018.
 */

public interface DataManager extends PreferencesHelper, DbHelper, ApiHelper {
}
