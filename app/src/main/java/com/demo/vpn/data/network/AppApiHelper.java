package com.demo.vpn.data.network;

import com.demo.vpn.BuildConfig;
import com.demo.vpn.data.network.model.DummyResponse;
import com.demo.vpn.data.network.model.IPAddress;
import com.demo.vpn.utils.HttpLogger;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by cedex on 1/23/2018.
 */
@Singleton
public class AppApiHelper implements ApiHelper{
    private ApiInterface apiInterface;
    private ApiInterface apiInterface2;

    @Inject
    public AppApiHelper() {

        Retrofit mRetrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(HttpLogger.getClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        Retrofit retrofit2 = new Retrofit.Builder()
                .baseUrl("https://api.ipify.org/")
                .client(HttpLogger.getClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        apiInterface = mRetrofit.create(ApiInterface.class);
        apiInterface2 = retrofit2.create(ApiInterface.class);
    }


    @Override
    public Observable<DummyResponse> getDummyResponse() {
        return apiInterface.getDummyContent();
    }

    @Override
    public Observable<IPAddress> getIpAddress() {
        return apiInterface2.getIpAddress("json");
    }
}
