package com.demo.vpn.data.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Aswin on 09-09-2018.
 */

public class IPAddress {
    @SerializedName("ip")
    private String ip;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
