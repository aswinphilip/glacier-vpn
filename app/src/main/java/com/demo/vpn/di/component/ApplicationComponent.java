package com.demo.vpn.di.component;

import android.app.Application;
import android.content.Context;

import com.demo.vpn.BaseApplication;
import com.demo.vpn.data.DataManager;
import com.demo.vpn.di.ApplicationContext;
import com.demo.vpn.di.module.ApplicationModule;
import com.demo.vpn.service.SyncService;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(BaseApplication application);

    void inject(SyncService service);

    @ApplicationContext
    Context context();

    Application application();

    DataManager getDataManager();

}
