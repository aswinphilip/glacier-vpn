package com.demo.vpn.di.module;

import android.app.Application;
import android.content.Context;

import com.demo.vpn.data.AppDataManager;
import com.demo.vpn.data.DataManager;
import com.demo.vpn.data.db.AppDbHelper;
import com.demo.vpn.data.db.DbHelper;
import com.demo.vpn.data.network.ApiHelper;
import com.demo.vpn.data.network.AppApiHelper;
import com.demo.vpn.data.pref.AppPreferencesHelper;
import com.demo.vpn.data.pref.PreferencesHelper;
import com.demo.vpn.di.ApplicationContext;
import com.demo.vpn.di.PreferenceInfo;
import com.demo.vpn.utils.AppConstants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
    private final Application mApplication;

    public ApplicationModule(Application mApplication) {
        this.mApplication = mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @Singleton
    DbHelper provideDbHelper(AppDbHelper appDbHelper) {
        return appDbHelper;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    @Singleton
    ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }

}
