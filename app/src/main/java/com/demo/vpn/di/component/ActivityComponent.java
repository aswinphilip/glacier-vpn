package com.demo.vpn.di.component;

import com.demo.vpn.di.PerActivity;
import com.demo.vpn.di.module.ActivityModule;
import com.demo.vpn.ui.MainActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class,modules = ActivityModule.class)
public interface ActivityComponent {

   void inject(MainActivity activity);
}
