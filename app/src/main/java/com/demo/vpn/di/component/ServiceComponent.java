package com.demo.vpn.di.component;

import android.net.VpnService;

import com.demo.vpn.di.PerService;
import com.demo.vpn.di.module.ServiceModule;
import com.demo.vpn.service.MyVpnService;
import com.demo.vpn.service.SyncService;

import dagger.Component;

@PerService
@Component(dependencies = ApplicationComponent.class,modules = ServiceModule.class)
public interface ServiceComponent {

    void inject(SyncService service);

    void inject(MyVpnService service);
}
