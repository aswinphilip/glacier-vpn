package com.demo.vpn.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.demo.vpn.di.ActivityContext;
import com.demo.vpn.di.PerActivity;
import com.demo.vpn.ui.MainMvpPresenter;
import com.demo.vpn.ui.MainMvpView;
import com.demo.vpn.ui.MainPresenter;
import com.demo.vpn.utils.rx.AppSchedulerProvider;
import com.demo.vpn.utils.rx.SchedulerProvider;

import org.greenrobot.eventbus.EventBus;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;


@Module
public class ActivityModule {
    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity mActivity) {
        this.mActivity = mActivity;
    }

    @Provides
    @ActivityContext
    Context provideContext(){
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    EventBus provideEventBus(){
        return EventBus.getDefault();
    }

    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView> providesMainMvpPresenter(MainPresenter<MainMvpView> presenter){
        return presenter;
    }
}
