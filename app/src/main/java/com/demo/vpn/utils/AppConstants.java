package com.demo.vpn.utils;

/**
 * Created by cedex on 1/23/2018.
 */

public class AppConstants {
    public static final String TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss";
    public static final int API_STATUS_CODE_LOCAL_ERROR = 0;
    public static final String PREF_NAME = "mvp.prefs";
}
